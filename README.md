# GETTING OVER IT (software studio final project)

    This game is run by cocos creator ver3.1.1, and is base on the famous game "Getting Over It with Bennett Foddy".

    The goal is to reach the flag at the end while using your cursor to adjust the Hammer and move the charactor forward.

    Not only that, we make a Two-Player mode using firebase and webRTC, and also make a Gacha system, let the player change their skin freely.

    Below is the description of the code implementation method

+ Game Link [here](https://ss-final-a91c4.web.app/)
+ Gitlab Page [here](https://gitlab.com/108062126/ss_final)

---

## Big picture:

The game have certain scene:
+ Menu:
    + This is the starting scene of the whole game
    + User can login by creating new account or by Google account.

+ ModeSelect:
    + Player can choose Single / Multi(Two) mode, and can also goto the store.
    + Single:
        + User can choose to start a new Game or start from the last record (if have)
    + Multi:
        + User can create Room or join existed room.

+ Store:
    + Here shows the player's stones
    + User can use thier stone to draw a new skin of charactor.

+ Play:
    + Two parts: main(single) & main-multi(Multi) scene.
    + Press Esc to open the option field
        + Adjusting volume
        + Resume
        + Quit game
            + Single mode will save the record to firebase and can restart by choosing last record.
            + Multi mode will delete the room, another player will get the msg and leave the room.
    + Button to trigger the trick (only Multi)

+ Finish:
    + Player's Own Leaderboard
    + World Ranking
    + Player can return back to ModeSelect

## Steps for developing
1. pull main_dev branch
2. make a new branch called "${work}_dev" or something similar
3. after complete that branch, back to this branch(main_dev)
4. pull AGAIN
5. merge your branch back to here
6. ADD, COMMIT, PUSH!!!


> Written by hchoHsu
