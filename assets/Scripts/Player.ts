
import { _decorator, Component, Node, TypeScript, IPhysics2DContact, Collider2D, Contact2DType, HingeJoint2D, Vec3, toRadian, EventKeyboard, systemEvent, SystemEventType, Vec2, EventMouse, RigidBody2D, SliderJoint2D, log, Vec4, Quat, find, resources, Texture2D, Sprite, SpriteFrame } from 'cc';
import {GameMgr} from './GameMgr'
import { Record } from './Record';
import { webRTC } from './webRTC';
const { ccclass, property } = _decorator;

@ccclass('Player')
export class Player extends Component {

    @property(Node)
    Game:Node | null = null;

    @property(Node)
    Joint:Node | null = null;

    @property(Node)
    Hammer: Node | null = null;

    game: GameMgr | null  = null;

    hinge: HingeJoint2D | null = null;

    slider: SliderJoint2D | null = null;

    record: Record | null = null;

    positionAligned: boolean = false;

    WebRTC: webRTC | null = null;

    goal: boolean = false;

    onLoad() {
        this.game = this.Game!.getComponent(GameMgr)!;
        this.hinge = this.node.getComponent(HingeJoint2D);
        this.slider = this.Joint!.getComponent(SliderJoint2D);
        this.record = find('Record')!.getComponent(Record);
        if (this.record!.isMulti){
            this.WebRTC = find('webRTC')!.getComponent(webRTC);
            let msg = {
                type: "SkinSet",
                num: this.record!.skinTextures[this.record!.skinIdx]._name
            }
            this.WebRTC!.SendData(JSON.stringify(msg));
        }
        this.goal = false;

        this.node.getComponent(Sprite)!.spriteFrame = this.record!.skinTextures[this.record!.skinIdx];
    }

    start () {
        log(this.record!.lastPosition!, this.record!.lastHammerPosition, this.record!.lastRotation!);
        let collider = this.getComponent(Collider2D);
        if (collider) {
            collider.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
        }
        if(this.record!.isMulti){
            if(this.record!.iscaller){
                this.node.setPosition(this.game!.callerPos!);
            }
            else {
                this.node.setPosition(this.game!.calleePos!);
            }
        }

    }

    update (dt: number) {
        //log(this.node.position, this.Joint!.position, this.Hammer!.position);
        this.hinge!.motorSpeed *= 0.5;
        this.slider!.motorSpeed *= 0.5;

        if(this.record!.hasRecord && this.record!.startLast && !this.positionAligned){
            if((new Vec3(this.record!.lastPosition!)).subtract(this.node.position).length() < 3){
                    this.node.getComponent(RigidBody2D)!.enabled = true;
                    this.Hammer!.getComponent(RigidBody2D)!.enabled = true;
                    this.Joint!.getComponent(RigidBody2D)!.enabled = true;
                    this.positionAligned = true;
            }
        }

        if(this.record!.hasRecord && this.record!.startLast && !this.positionAligned){
            log("aligning");
            this.node.getComponent(RigidBody2D)!.enabled = false;
            this.Hammer!.getComponent(RigidBody2D)!.enabled = false;
            this.Joint!.getComponent(RigidBody2D)!.enabled = false;

            let t = (new Vec3(this.record!.lastPosition!)).add3f(0, 2, 0);
            this.node.setPosition(t);
            let r = this.record!.lastRotation!.z;
            if(this.record!.lastRotation!.x != 0){
                r = 180 - r;
            }
            this.Joint!.setRotationFromEuler(0, 0, r);
            this.Joint!.setPosition(new Vec3(0, 0, 0));
            this.Hammer!.setPosition(this.record!.lastHammerPosition!);
            //this.positionAligned = true;
        }

        let radius = new Vec3(this.Hammer!.position);
        let len = radius.length();
        if(this.game!.deltaPos.length() > 0){

            //rotate
            let deltaRadius = (new Vec3(radius)).add3f(this.game!.deltaPos.x, this.game!.deltaPos.y, 0).normalize();
    
            let dir = new Vec3(radius).cross(new Vec3(deltaRadius));
            let deltaRad = Math.acos(deltaRadius.dot((new Vec3(radius).normalize())));

            if(dir.z > 0){
                this.hinge!.motorSpeed = deltaRad * len / 8;
            }
            else {
                this.hinge!.motorSpeed = -deltaRad * len / 8;
            }

            //slide
            let dSlide = (new Vec3(this.game!.deltaPos.x, this.game!.deltaPos.y, 0)).normalize().dot(radius.normalize());
            if(Math.abs(dSlide) > 0.3){
                this.hinge!.maxMotorTorque = (1 - Math.abs(dSlide)) * 100;
                this.slider!.maxMotorForce = Math.abs(dSlide) * 100;

                this.slider!.motorSpeed = dSlide * 4;
            }
        }
        
        if(this.record!.isMulti && this.WebRTC!.connected){
            let rot = this.Joint!.getRotation(new Quat()).getEulerAngles(new Vec3());
            let sendMsg = {
                type: 'Player',
                delta: {
                    x: this.game!.deltaPos.x,
                    y: this.game!.deltaPos.y,
                },
                position: {
                    x: this.node.position.x,
                    y: this.node.position.y,
                },
                rotation: {
                    x: rot.x,
                    y: rot.y,
                    z: rot.z
                },
                hammerPos: {
                    x: this.Hammer!.position.x,
                    y: this.Hammer!.position.y
                },
                goal: this.goal
            }
            this.WebRTC!.SendData(JSON.stringify(sendMsg));
        }

        this.game!.deltaPos = new Vec2();
    }

    onBeginContact (self:Collider2D, other:Collider2D, contact:IPhysics2DContact){
        if(other.node.name == 'Hammer'){
            contact.disabled = true;
        }
        else if(other.node.name == 'flag'){
            contact.disabled = true;
            this.goal = true;
        }
        else if(other.node.name == 'ground'){
            if(this.record!.isMulti){
                if(this.record!.iscaller == false){
                    this.node.setPosition(3000, -300, 0);
                }
            }
        }
    }
}