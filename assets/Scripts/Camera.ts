
import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Camera')
export class Camera extends Component {
    
    @property(Node)
    Player: Node | null = null;

    update(dt:number){
        this.node.setPosition(this.Player!.position.x, this.Player!.position.y, 1000);
    }
}
