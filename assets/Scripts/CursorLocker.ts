
import { _decorator, Component, Node, Vec2, game, TERRAIN_HEIGHT_BASE, find } from 'cc';
import { GameMgr } from './GameMgr';
const { ccclass, property } = _decorator;

@ccclass('CursorLocker')
export class CursorLocker extends Component {
    
    Game: Node | null = null;

    Options: Node | null = null;

    Intro: Node | null = null;

    elem: any = null;

    locked: boolean = false;

    started: boolean = false;

    inGame: boolean = false;

    onLoad() {
        game.addPersistRootNode(this.node);
    }

    start() {
        let that = this;
        document.addEventListener("mousemove", function(e) {
            var movementX = e.movementX       ||
                            e.mozMovementX    ||
                            e.webkitMovementX ||
                            0,
                movementY = e.movementY       ||
                            e.mozMovementY    ||
                            e.webkitMovementY ||
                            0;
        
            // Print the mouse movement delta values
            //console.log("movementX=" + movementX, "movementY=" + movementY);
            if(that.locked && that.inGame && that.Game){
                that.Game!.getComponent(GameMgr)!.deltaPos = new Vec2(movementX, -movementY);
            }
        }, false);

        document.addEventListener('pointerlockchange', ()=>{
            that.locked = !that.locked;
            if(that.started){
                that.Options!.active = !(that.Options!.active);
            }
            else {
                that.Game!.getComponent(GameMgr)!.started = true;
                that.started = true;
                that.Intro!.active = false;
            }
            if (document.pointerLockElement === that.elem ||
                document.mozPointerLockElement === that.elem ||
                document.webkitPointerLockElement === that.elem) {
                console.log("Pointer Lock was successful.");
            } else {
                console.log("Pointer Lock was lost.");
            }
        }, false);
        document.addEventListener('mozpointerlockchange', ()=>{
            that.locked = !that.locked;
            if(that.started){
                that.Options!.active = !(that.Options!.active);
            }
            else {
                that.Game!.getComponent(GameMgr)!.started = true;
                that.started = true;
                that.Intro!.active = false;
            }
            if (document.pointerLockElement === that.elem ||
                document.mozPointerLockElement === that.elem ||
                document.webkitPointerLockElement === that.elem) {
                console.log("Pointer Lock was successful.");
            } else {
                console.log("Pointer Lock was lost.");
            }
        }, false);
        document.addEventListener('webkitpointerlockchange', ()=>{
            that.locked = !that.locked;
            if(that.started){
                that.Options!.active = !(that.Options!.active);
            }
            else {
                that.Game!.getComponent(GameMgr)!.started = true;
                that.started = true;
                that.Intro!.active = false;
            }
            if (document.pointerLockElement === that.elem ||
                document.mozPointerLockElement === that.elem ||
                document.webkitPointerLockElement === that.elem) {
                console.log("Pointer Lock was successful.");
            } else {
                console.log("Pointer Lock was lost.");
            }
        }, false);

        document.addEventListener('pointerlockerror', this.pointerLockError, false);
        document.addEventListener('mozpointerlockerror', this.pointerLockError, false);
        document.addEventListener('webkitpointerlockerror', this.pointerLockError, false);

    }

    pointerLockError() {
        console.log("Error while locking pointer.");
    }

    lockPointer() {
        console.log('lock');
        if(!this.elem){
            this.elem = document.getElementById("GameDiv");
            this.elem.requestPointerLock = this.elem.requestPointerLock    ||
                              this.elem.mozRequestPointerLock ||
                              this.elem.webkitRequestPointerLock;
        }
        // Start by going fullscreen with the element. Current implementations
        // require the element to be in fullscreen before requesting pointer
        // lock--something that will likely change in the future.

        this.elem.requestPointerLock();
    }

    reconnectNode() {
        this.Game = find('Canvas');
        this.Options = find('Canvas/Options');
        this.Intro = find('Canvas/Camera/Intro');
    }

    setStart(start:boolean){
        this.started = false;
        this.locked = false;
        if(start){
            this.reconnectNode();
            this.inGame = true;
        }
        else {
            this.inGame = false;
        }
    }
}
