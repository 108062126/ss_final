
import { _decorator, Component, Node, game, Vec2, Vec3, Quat, Texture2D} from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Record')
export class Record extends Component {

    lastTime: number = 0;
    lastPosition: Vec3 | null = null;
    lastRotation: Vec3 | null = null;
    lastHammerPosition: Vec3 | null = null;
    hasRecord: boolean = false;
    
    stones: number = 0;
    displayName: string = '';
    infoLoaded: boolean = false;

    startLast: boolean = false;
    time: number = 0;

    roomId: string = "";
    isMulti: boolean = false;
    iscaller: boolean = false;

    player1: string = "";
    player2: string = "";

    skins: any = null;
    skinIdx: number = 0;
    skinTextures: any = null;

    onLoad() {
        game.addPersistRootNode(this.node);
        this.skins = new Array();
        this.skinTextures = new Array();
    }
}
