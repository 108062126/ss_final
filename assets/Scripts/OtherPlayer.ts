import { _decorator, Component, Node, TypeScript, IPhysics2DContact, Collider2D, Contact2DType, HingeJoint2D, Vec3, toRadian, EventKeyboard, systemEvent, SystemEventType, Vec2, EventMouse, RigidBody2D, SliderJoint2D, log, Vec4, Quat, find, SpriteFrame, resources, Sprite } from 'cc';
import {GameMgr} from './GameMgr'
import { Record } from './Record';
import { webRTC } from './webRTC';
const { ccclass, property } = _decorator;

@ccclass('OtherPlayer')
export class OtherPlayer extends Component {
    
    @property(Node)
    Joint:Node | null = null;

    @property(Node)
    Hammer: Node | null = null;

    @property(Node)
    Game: Node | null = null;

    game: GameMgr | null  = null;

    hinge: HingeJoint2D | null = null;

    slider: SliderJoint2D | null = null;

    positionAligned: boolean = false;

    lastPos: Vec3 | null = new Vec3();

    lastRot: Vec3 | null = new Vec3();

    lastHammerPos: Vec3 | null = new Vec3();

    posRegression: boolean = false;

    inputQueue: any | null = null;

    record: Record | null = null;

    goal: boolean | boolean = false;

    webRTC: webRTC | null = null;

    onLoad() {
        this.hinge = this.node.getComponent(HingeJoint2D);
        this.slider = this.Joint!.getComponent(SliderJoint2D);
        this.game = this.Game!.getComponent(GameMgr);
        this.lastPos = this.node.position;
        this.lastRot = new Vec3(180, 0, 0);
        this.lastHammerPos = this.Hammer!.position;
        this.inputQueue = new Array();
        this.record = find('Record')!.getComponent(Record);
        this.webRTC = find('webRTC')!.getComponent(webRTC);
        this.goal = false;
    }

    start () {
        if(this.record!.isMulti){
            if(this.record!.iscaller){
                this.node.setPosition(this.game!.calleePos!);
            }
            else {
                this.node.setPosition(this.game!.callerPos!);
            }
        }
        this.setSkinFromWebRTC();
    }

    update (dt: number) {
        this.hinge!.motorSpeed *= 0.5;
        this.slider!.motorSpeed *= 0.5;

        let dPos = (new Vec3(this.node.position)).subtract(this.lastPos!).length();
        let dHammerPos = (new Vec3(this.Hammer!.position)).subtract(this.lastHammerPos!).length();

        if(dPos > 3 || dHammerPos > 3){
            if(dPos > 3){
                this.node.setPosition(this.lastPos!);
            }
            if(dHammerPos > 3){
                let r = this.lastRot!.x != 0 ? 180 - this.lastRot!.z : this.lastRot!.z;
                this.Joint!.setRotationFromEuler(new Vec3(0, 0, r));
                this.Hammer!.setPosition(this.lastHammerPos!);
            }
        }
        if(this.inputQueue.length > 0){
            let deltaPos = this.inputQueue.shift();
            let radius = new Vec3(this.Hammer!.position);
            let len = radius.length();
            if(deltaPos.length() > 0){
            
                //rotate
                let deltaRadius = (new Vec3(radius)).add3f(deltaPos.x, deltaPos.y, 0).normalize();
            
                let dir = new Vec3(radius).cross(new Vec3(deltaRadius));
                let deltaRad = Math.acos(deltaRadius.dot((new Vec3(radius).normalize())));
            
                if(dir.z > 0){
                    this.hinge!.motorSpeed = deltaRad * len / 8;
                }
                else {
                    this.hinge!.motorSpeed = -deltaRad * len / 8;
                }
            
                //slide
                let dSlide = (new Vec3(deltaPos.x, deltaPos.y, 0)).normalize().dot(radius.normalize());
        
                if(Math.abs(dSlide) > 0.3){
                    this.hinge!.maxMotorTorque = (1 - Math.abs(dSlide)) * 100;
                    this.slider!.maxMotorForce = Math.abs(dSlide) * 100;
        
                    this.slider!.motorSpeed = dSlide * 5;
                }
            }
        }
    }

    onBeginContact (self:Collider2D, other:Collider2D, contact:IPhysics2DContact){
        if(other.node.name == 'Hammer'){
            contact.disabled = true;
        }
    }

    setSkin(num: number){
        log('set skin: ', num);
        resources.load("images/" + num.toString() + "/spriteFrame", SpriteFrame ,(err: any, spFrame: SpriteFrame) => {
            this.node.getComponent(Sprite)!.spriteFrame = spFrame;
        });
    }

    setSkinFromWebRTC(){
        let that = this;
        let cb = function(){
            if(that.webRTC!.otherPlayerSkinLoaded){
                that.node.getComponent(Sprite)!.spriteFrame = that.webRTC!.tempOtherPlayerSkin;
                that.unschedule(cb);
            }
        }
        this.schedule(cb, 0.1);
    }

    updateInput(input: any){
        this.goal = input.goal;

        let v = new Vec2(input.delta.x, input.delta.y);
        if(v.length() > 0){
            this.inputQueue.push(new Vec2(input.delta.x, input.delta.y));
        }
        this.lastPos = new Vec3(input.position.x, input.position.y, 0);
        this.lastRot = new Vec3(input.rotation.x, input.rotation.y, input.rotation.z);
        this.lastHammerPos = new Vec3(input.hammerPos.x, input.hammerPos.y, 0);
    }
}
