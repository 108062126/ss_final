import { _decorator, Component, Node, find, EditBox, Button, Label, EventHandler, director, Prefab, instantiate, Vec2, loader, AudioSource, AudioSourceComponent, AudioClip } from 'cc';
import { Record } from './Record';
const { ccclass, property } = _decorator;

@ccclass('Finish')
export class Finish extends Component {

    /* Basic Nodes */
    @property(Button)   btnReturn: Button | null = null;

    @property(Node) selfRecordBoard: Node | null = null;
    @property(Node) globalRecordBoard: Node | null = null;

    @property({type: Prefab})
    selfRecordPrefab: Prefab | null = null;
    @property({type: Prefab})
    globalRecordPrefab: Prefab | null = null;

    @property(Node)
    gainStone: Node | null = null;

    @property(AudioClip)
    windSound: AudioClip | null = null;

    @property(Label)    TimeLabel: Label | null = null;

    record: Record | null = null;

    onLoad () {
        this.record = find('Record')!.getComponent(Record);
        if (!this.record!.isMulti){
            let uid = firebase.auth().currentUser.uid;
            firebase.database().ref('userInfo/' + uid + '/hasRecord').set(0);
            this.record!.hasRecord = false;
            this.loadSelfRecord();
            this.getStone(3);
        }
        else {
            this.getStone(5);
        }
        this.loadGlobalRecord();
        this.TimeLabel!.string = 'Time: ' + Math.floor(this.record!.time).toString();
    }

    start () {
        this.btnReturn!.node.on('click', this.Return, this);
        this.record!.getComponent(AudioSource)!.playOneShot(this.windSound!, 0.2);
    }

    getStone(num: number){
        this.scheduleOnce(()=>{
            this.gainStone!.active = true;
        }, 3);

        this.gainStone!.getChildByName('Label')!.getComponent(Label)!.string = '+' + num.toString();

        this.record!.stones += num;
        let uid = firebase.auth().currentUser.uid;
        firebase.database().ref('userInfo/' + uid + '/stones').set(this.record!.stones);

    }

    private loadSelfRecord () {
        const userID = firebase.auth().currentUser.uid;
        const roomRef = firebase.database().ref('userInfo/' + userID + '/selfRecord');

        let startPosition = 87, diff = 58.5;
        let newRecord = null;
        roomRef.orderByChild('time').limitToFirst(5).once('value', (snapshot: any) => {
            snapshot.forEach((score: any) => {
                newRecord = instantiate(this.selfRecordPrefab);
                newRecord!.parent = this.selfRecordBoard;

                newRecord!.setPosition(0, startPosition, 0);
                newRecord!.getComponent(Label)!.string = Math.floor(score.val().time).toString() + " s";

                startPosition -= diff;
            })
        });
    }

    private loadGlobalRecord () {
        /* Change the ranking path via record.isMulti */
        let path: string = "";
        if (this.record!.isMulti) {
            path = "RankingMulti";
            this.selfRecordBoard!.active = false;
            this.globalRecordBoard!.setPosition(480, 310, 0);
        }
        else path = "Ranking";

        /* Set Path */
        const roomRef = firebase.database().ref(path);

        /* Show Rank */
        let startPosition = 87, diff = 58.5;
        let newRecord = null;
        roomRef.orderByChild('time').limitToFirst(5).once('value', (snapshot: any) => {
            if (!snapshot.exists()) return;

            snapshot.forEach((score: any) => {
                newRecord = instantiate(this.globalRecordPrefab);
                newRecord!.parent = this.globalRecordBoard;
                newRecord!.setPosition(0, startPosition, 0);

                const data = score.val();
                newRecord.getChildByName("name")!.getComponent(Label)!.string
                    = data.player.split('@')[0];
                newRecord.getChildByName("time")!.getComponent(Label)!.string
                    = Math.floor(data.time).toString() + " s";

                startPosition -= diff;
            })
        });
    }

    private Return(button: Button){
        director.loadScene("ModeSelect");
    }
}
