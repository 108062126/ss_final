import { _decorator, Component, Node, log, Vec3, Vec2,IPhysics2DContact, Collider2D,Contact2DType, EventMouse, systemEvent, SystemEventType, UITransform, Script, TypeScript, RigidBody2D, Slider, AudioSource, AudioClip, director, Director, find, Button, EventHandler, Quat, v2} from 'cc';
import { Hammer } from '../Hammer'
import { Player } from '../Player';
import { Channel } from './Channel';
const { ccclass, property } = _decorator;

@ccclass('Mechanism')
export class Mechanism extends Component {

    @property(Node)
    Mechanism_Channel: Node | null = null;

    @property(Node)
    Player: Node | null = null;

    @property(Node)
    Hammer: Node | null = null;

    fan1_On: boolean = false;

    rock1_On: boolean = false;

    elevator1_1: boolean = false;

    elevator1_2: boolean = false;

    elevator1_3: boolean = false;

    elevator2_1: boolean = false;

    elevator2_2: boolean = false;

    elevator2_3: boolean = false;
    

    start () {
        let collider = this.getComponent(Collider2D);
        if (collider) {
            collider.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
            collider.on(Contact2DType.END_CONTACT, this.onEndContact, this);
        }
    }

    onBeginContact (self:Collider2D, other:Collider2D, contact:IPhysics2DContact){
        if(other.node.name == 'Player'){
            if(self.node.name == 'botton1_1'){
                this.fan1_On = true;
                this.Mechanism_Channel!.getComponent(Channel)!.fan1_On = this.fan1_On;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'botton1_2' || self.node.name == 'botton1_3'){
                this.rock1_On = true;
                this.Mechanism_Channel!.getComponent(Channel)!.rock1_On = this.rock1_On;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
                //console.log(this.rock1_On);
            }
            else if(self.node.name == 'bottone1_1' || self.node.name == 'bottone3_1'){
                this.elevator2_1 = true;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator2_1 = this.elevator2_1;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone1_2' || self.node.name == 'bottone3_2'){
                this.elevator2_2 = true;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator2_2 = this.elevator2_2;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone1_3' || self.node.name == 'bottone3_3'){
                this.elevator2_3 = true;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator2_3 = this.elevator2_3;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone2_1'){
                this.elevator1_1 = true;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator1_1 = this.elevator1_1;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone2_2'){
                this.elevator1_2 = true;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator1_2 = this.elevator1_2;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone2_3'){
                this.elevator1_3 = true;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator1_3 = this.elevator1_3;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
        }
    }

    onEndContact(self:Collider2D, other:Collider2D, contact:IPhysics2DContact){
        if(other.node.name == 'Player'){
            if(self.node.name == 'botton1_1'){
                // this.fan1_On = false;
                // this.Mechanism_Channel!.getComponent(Channel)!.fan1_On = this.fan1_On;
                // this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'botton1_2' || self.node.name == 'botton1_3'){
                this.rock1_On = false;
                this.Mechanism_Channel!.getComponent(Channel)!.rock1_On = this.rock1_On;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone1_1' || self.node.name == 'bottone3_1'){
                this.elevator2_1 = false;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator2_1 = this.elevator2_1;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone1_2' || self.node.name == 'bottone3_2'){
                this.elevator2_2 = false;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator2_2 = this.elevator2_2;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone1_3' || self.node.name == 'bottone3_3'){
                this.elevator2_3 = false;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator2_3 = this.elevator2_3;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone2_1'){
                this.elevator1_1 = false;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator1_1 = this.elevator1_1;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone2_2'){
                this.elevator1_2 = false;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator1_2 = this.elevator1_2;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
            else if(self.node.name == 'bottone2_3'){
                this.elevator1_3 = false;
                this.Mechanism_Channel!.getComponent(Channel)!.elevator1_3 = this.elevator1_3;
                this.Mechanism_Channel!.getComponent(Channel)!.Send_Msg();
            }
        }
    }

}
