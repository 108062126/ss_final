import { _decorator, Component, Node, IPhysics2DContact, Collider2D,Contact2DType, RigidBody2D, v2} from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Fan')
export class Fan extends Component {
    
    @property(Node)
    Player: Node | null = null;

    fan1_On: boolean = false;

    start () {
        let collider = this.getComponent(Collider2D);
        if (collider) {
            collider.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
        }
    }

    onBeginContact (self:Collider2D, other:Collider2D, contact:IPhysics2DContact){
        if(other.node.name == 'Player'){
            if(this.fan1_On)
                this.fan();
        }
    }

    fan(){
        this.Player!.getComponent(RigidBody2D)!.linearVelocity = v2(0,30);
    }

}
