import { _decorator, Component, Node, log, Vec3, Vec2,IPhysics2DContact, Collider2D,Contact2DType, EventMouse, systemEvent, SystemEventType, UITransform, Script, TypeScript, RigidBody2D, Slider, AudioSource, AudioClip, director, Director, find, Button, EventHandler, Quat, v2} from 'cc';
import { Hammer } from '../Hammer'
import { Player } from '../Player';
import { Record } from '../Record';
import { CursorLocker } from '../CursorLocker';
import { Fan } from './Fan';
import { Rock } from './Rock';
import { Elevator } from './Elevator';
import { webRTC } from '../webRTC';
const { ccclass, property } = _decorator;

@ccclass('Channel')
export class Channel extends Component {
    @property(Node)
    Player: Node | null = null;

    @property(Node)
    Hammer: Node | null = null;

    @property(Node)
    Fan1: Node | null = null;

    @property(Node)
    Rock1: Node | null = null;

    @property(Node)
    Elevator1: Node | null = null;

    @property(Node)
    Elevator2: Node | null = null;

    fan1_On: boolean = false;

    rock1_On: boolean = false;

    elevator1_1: boolean = false;

    elevator1_2: boolean = false;

    elevator1_3: boolean = false;

    elevator2_1: boolean = false;

    elevator2_2: boolean = false;

    elevator2_3: boolean = false;

    WebRTC: webRTC | null = null;

    onLoad(){
        this.WebRTC = find('webRTC')!.getComponent(webRTC);
    }

    update () {
        this.Call_Fan();
        this.Call_Rock();
        this.Call_Elevator1();
        this.Call_Elevator2();
    }

    Send_Msg(){
        if(this.WebRTC!.connected){
            let sendMsg = {
                type: 'Mechanism',
                fan: {
                    fan1_On: this.fan1_On
                },
                rock: {
                    rock1_On: this.rock1_On
                },
                elevator1: {
                    l1: this.elevator1_1,
                    l2: this.elevator1_2,
                    l3: this.elevator1_3
                },
                elevator2: {
                    l1: this.elevator2_1,
                    l2: this.elevator2_2,
                    l3: this.elevator2_3
                }
            }
            this.WebRTC!.SendData(JSON.stringify(sendMsg));
        }
    }

    Receive_Msg(input: any){
        this.fan1_On = input.fan.fan1_On;
        this.rock1_On = input.rock.rock1_On;
        this.elevator1_1 = input.elevator1.l1;
        this.elevator1_2 = input.elevator1.l2;
        this.elevator1_3 = input.elevator1.l3;
        this.elevator2_1 = input.elevator2.l1;
        this.elevator2_2 = input.elevator2.l2;
        this.elevator2_3 = input.elevator2.l3;
    }

    
    Call_Fan(){
        this.Fan1!.getComponent(Fan)!.fan1_On = this.fan1_On;
    }

    Call_Rock(){
        if (this.rock1_On) {
            this.Rock1!.active = true;
        }
        else {
            this.Rock1!.active = false;
        }
    }

    Call_Elevator1(){
        this.Elevator1!.getComponent(Elevator)!.Elevator_1 = this.elevator1_1;
        this.Elevator1!.getComponent(Elevator)!.Elevator_2 = this.elevator1_2;
        this.Elevator1!.getComponent(Elevator)!.Elevator_3 = this.elevator1_3;
        this.Elevator1!.getComponent(Elevator)!.ElevatorChangeFloor();
    }

    Call_Elevator2(){
        this.Elevator2!.getComponent(Elevator)!.Elevator_1 = this.elevator2_1;
        this.Elevator2!.getComponent(Elevator)!.Elevator_2 = this.elevator2_2;
        this.Elevator2!.getComponent(Elevator)!.Elevator_3 = this.elevator2_3;
        this.Elevator2!.getComponent(Elevator)!.ElevatorChangeFloor();
    }
}
