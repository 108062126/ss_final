import { _decorator, Component} from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Rock')
export class Rock extends Component {

    onLoad () {
        console.log('active false');
        this.node.active = false;
    }

    rockOn(){
        this.node.active = true;
    }
    
    rockDown(){
        this.node.active = false;
    }
}
