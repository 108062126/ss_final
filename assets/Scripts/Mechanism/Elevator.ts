import { _decorator, Component, Node,  log, Vec3, Vec2,IPhysics2DContact, Collider2D,Contact2DType, EventMouse, systemEvent, SystemEventType, UITransform, Script, TypeScript, RigidBody2D, Slider, AudioSource, AudioClip, director, Director, find, Button, EventHandler, Quat, v2, tween, v3} from 'cc';
import { Hammer } from '../Hammer'
import { Player } from '../Player';
import { Record } from '../Record';
import { Mechanism  } from './Mechanism';
import { Channel } from './Channel';
const { ccclass, property } = _decorator;

@ccclass('Elevator')
export class Elevator extends Component {

    Elevator_1: boolean = false;

    Elevator_2: boolean = false;

    Elevator_3: boolean = false;

    //floor: 1 2 3 ->y = 850 1350 1950

    start () {        
    }

    ElevatorChangeFloor(){
        if(this.Elevator_1 == true && this.Elevator_2 == false && this.Elevator_3 == false){
            tween(this.node).to(2, { position: v3(this.node!.position.x, 850, this.node!.position.z )}).start();
        }
        else if(this.Elevator_1 == false && this.Elevator_2 == true && this.Elevator_3 == false){
            tween(this.node).to(2, { position: v3(this.node!.position.x, 1350, this.node!.position.z )}).start();
        }
        else if(this.Elevator_1 == false && this.Elevator_2 == false && this.Elevator_3 == true){
            tween(this.node).to(2, { position: v3(this.node!.position.x, 1950, this.node!.position.z )}).start();
        }
    }
    

}
