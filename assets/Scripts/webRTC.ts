/* webRTC.ts */
// Position: Hall.scene/webRTC

/* =============== Information ================
    Using webRTC to create a faster connection between players.
    
    Offer 2 functions:
        SendData(your_data) -- Use the function to Send the data to remote side.
        ReceiveData()       -- Receive the remote data, identify the target of the data.

    Warning: The function only works when "this.connected" is set to be TRUE.
 ============================================= */

import { _decorator, Component, Node, Label, find, game, director, SpriteFrame, resources } from 'cc';
import { Record } from './Record';
import { HallUI } from './Hall/HallUI'
import { OtherPlayer } from './OtherPlayer';
import { Channel } from './Mechanism/Channel';
const { ccclass, property } = _decorator;

@ccclass('webRTC')
export class webRTC extends Component {

    /* Basic Nodes */
    public connected: boolean = false;

    @property(Label) RoomID: Label | null = null;
    @property(Node) Hall: Node | null = null;

    record: Record | null = null;
    hallUI: HallUI | null = null;
    otherPlayer: OtherPlayer | null = null;
    mechanism: Channel | null = null;
    Remote_Goal: boolean | boolean = false;

    private inGame: boolean | boolean = false;
    private pc: any | null = null;             // PeerConnections
    private datachannel: any | null = null;    // datachannel
    private config: any | null = null;         // ICE path congiguration
    private roomId: string | string = "";

    tempOtherPlayerSkin: SpriteFrame | null = null;
    otherPlayerSkinLoaded: boolean = false;

    @property(Boolean) DEBUG: Boolean | Boolean = false;


    /* ================= Send and Receive ================
       =================================================== */

    public ReceiveData(remoteData: any) {
        const data = JSON.parse(remoteData);

        switch (data.type) {
            case 'Start':
                this.hallUI!.changeScene();
                break;
            case 'Ready':
                this.hallUI!.setReady();
                this.hallUI!.setGuest(data.email);
                break;
            case 'Player':
                if (this.otherPlayer && this.inGame)
                    this.otherPlayer!.updateInput(data);
                break;
            case 'Mechanism':
                if (this.mechanism && this.inGame)
                    this.mechanism!.Receive_Msg(data);
                break;
            case 'SkinSet':
                this.loadOtherPlayerSkin(data.num);
                break;
            case 'victory':
                this.Remote_Goal = true;
                break;
        }
    }
    public SendData(data: any) {
        if (this.connected == false)
            return;
        if (JSON.parse(data).type != "Player")
            console.log(data);
        
        this.datachannel.send(data);
    }
    public setInGame() {
        this.otherPlayer = find('Canvas/OtherPlayer')!.getComponent(OtherPlayer);
        this.mechanism   = find('Canvas/TiledMap/Mechanism')!.getComponent(Channel);
        game.removePersistRootNode(this.node);
        
        this.inGame = true;
        console.log('webRTC in Game: ', this.inGame);
    }

    /* =================== Life Cycle ====================
       =================================================== */

    onLoad() {
        game.addPersistRootNode(this.node);
        this.hallUI = this.Hall!.getComponent(HallUI);
        this.record = find('Record')!.getComponent(Record);

        this.Candidate_Stack = new Array();
    }
    start() {
        this.config = {    // Set ICE path
            iceServers: [
                {
                    urls: [ // google public STUN servers
                        'stun:stun1.l.google.com:19302',
                        'stun:stun2.l.google.com:19302',
                    ]
                },
                {
                    url: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                }
            ],
            iceCandidatePoolSize: 2
        };

        // Set webRTC connection
        if (this.record!.iscaller)
            this.createRoom();
        else
            this.joinRoom();
    }
    onDestroy() {
        this.disconnectRoom();
    }
    private disconnectRoom() {
        this.inGame = false;
        this.connected = false;

        if (this.datachannel)
            this.datachannel.close();

        if (this.roomId) {
            let path = this.roomId;
            firebase.database().ref('rooms').once('value').then((snapshot: any) => {
                if (snapshot.exists() && snapshot.child(path).exists()) {
                    firebase.database().ref('rooms/' + path).remove();
                    console.log('Destroy Room');
                }
            })
        }
    }

    loadOtherPlayerSkin(idx: number){
        resources.load("images/" + idx.toString() + "/spriteFrame", SpriteFrame ,(err: any, spFrame: SpriteFrame) => {
            this.tempOtherPlayerSkin = spFrame;
            this.otherPlayerSkinLoaded = true;
        });
    }
    /* ================ WebRTC Connection ================
       =================================================== */

    private async createRoom() {
        const roomRef = await firebase.database().ref('rooms');


        /* Create a new PeerConnection */
        this.pc = new RTCPeerConnection(this.config);
        this.registerPCListeners();

        this.pc.addEventListener('icecandidate', async(event: any) => {
            if (event.candidate) {
                console.log('Got candidate: ', event.candidate);
                await firebase.database().ref('rooms/' + this.roomId + '/callerCandidates').push(event.candidate.toJSON());
            } else {
                //  console.log('Got Final candidate!');
            }
        })

        /* Setting Local DataChannel */
        this.datachannel = this.pc.createDataChannel('sendDataChannel');
        this.registerDCListeners();
        this.datachannel.addEventListener('message', (event: any) => {
            const data = event.data;
            this.ReceiveData(data);
        })

        /* Create offer and add to connection */
        const offer = await this.pc.createOffer();
        await this.pc.setLocalDescription(offer);
        const roomWithOffer = {
            'offer': {
                type: offer.type,
                sdp: offer.sdp
            },
            'ownner': firebase.auth().currentUser.email
        };
        this.roomId = roomRef.push().key;   // Set room ID
        console.log('Room Id: ', this.roomId);
        roomRef.child(this.roomId).set(roomWithOffer);

        /* Setting ICE candidates */

        /* Listen to the remote answer & candidates */
        firebase.database().ref('rooms/' + this.roomId).on('value', async (snapshot: any) => {
            const data = snapshot.val();
            if (data && !this.pc.currentRemoteDescription && data.answer) {
                console.log('remote answer');
                await this.pc.setRemoteDescription(new RTCSessionDescription(data.answer));
                //console.log('Got remote description: ', data.answer);
            }
        });

        firebase.database().ref('rooms/' + this.roomId + '/calleeCandidates').on('child_added', async(snapshot: any, prevChildKey: any) => {
            //sanpshot.forEach(async (change: any) => {
            let change = snapshot;
            if (change.val()) {
                let data = change.val();
                await this.pc.addIceCandidate(new RTCIceCandidate(data));
                console.log("Got remote candidate:" + JSON.stringify(data));
            }
            //});
        });

        /* Update the Label and Record */
        this.RoomID!.string = "Room ID: " + this.roomId;
        this.record!.roomId = this.roomId;
    }
    private async joinRoom() {
        /* Get the roomId from Record */
        this.roomId = this.record!.roomId;
        this.RoomID!.string = "Rood ID: " + this.record!.roomId;
        const roomRef = firebase.database().ref('rooms/' + `${this.roomId}`);

        /* Get remote data */
        let roomSnapShot: any;
        await roomRef.once('value', (snapshot: any) => {
            roomSnapShot = snapshot.val();
        })

        /* Create a new PeerConnection */
        this.pc = new RTCPeerConnection(this.config);
        this.registerPCListeners();

        /* Listen Remote DataChannel */
        this.pc.addEventListener('datachannel', (event: any) => {
            this.datachannel = event.channel;
            this.datachannel.addEventListener('message', (event: any) => {
                const data = event.data;
                this.ReceiveData(data);
            }); this.registerDCListeners();
        })

        /* Setting Offer and Answer Key */
        const offer = roomSnapShot.offer;
        await this.pc.setRemoteDescription(new RTCSessionDescription(offer));
        const answer = await this.pc.createAnswer();
        await this.pc.setLocalDescription(answer);

        const roomWithAnswer = {
            answer: {
                type: answer.type,
                sdp: answer.sdp
            }
        };
        await roomRef.update(roomWithAnswer);

        /* Setting  ICE candidates */
        this.pc.addEventListener('icecandidate', (event: any) => {
            if (event.candidate) {
                console.log('Got candidate: ', event.candidate);
                firebase.database().ref('rooms/' + this.roomId + '/calleeCandidates').push(event.candidate.toJSON());
            }
        });

        /* Listen to remote ice candidate */
        firebase.database().ref('rooms/' + this.roomId + '/callerCandidates').on('child_added', async (snapshot: any,  prevChildKey: any) => {
            if (snapshot.val()) {
                let data = snapshot.val();
                await this.pc.addIceCandidate(new RTCIceCandidate(data));
                console.log("Got Remote callercandiates:" + JSON.stringify(data));
            }
        });
    }

    /* Down below main for testing confirm, but this.connected is setted here */
    private registerPCListeners() {
        let self = this;
        this.pc.addEventListener('icegatheringstatechange', () => {
            if (self.pc)
                console.log(`ICE gathering state changed: ${self.pc.iceGatheringState}`);
        })

        this.pc.addEventListener('connectionstatechange', () => {
            if (self.pc) {
                console.log(`Connection state change: ${self.pc.connectionState}`);
                if (self.pc.connectionState == "failed") {
                    alert("Oops!, There is something wrong for your browser or internet, please try Create another Room or change the Caller and Callee.");
                    game.removePersistRootNode(self.node);
                    director.loadScene('ModeSelect');
                }
            }
        });

        this.pc.addEventListener('signalingstatechange', () => {
            if (self.pc) 
                console.log(`Signaling state change: ${self.pc.signalingState}`);
        });

        this.pc.addEventListener('iceconnectionstatechange ', () => {
            if (self.pc)
                console.log(`ICE connection state change: ${self.pc.iceConnectionState}`);
        });
    }
    private registerDCListeners() {
        let self = this;
        this.datachannel.addEventListener('open', (event: any) => {
            console.log(`datachannel state changed: ` + self.datachannel.readyState);
            this.connected = true;
        })

        this.datachannel.addEventListener('close', (event: any) => {
            if (self.datachannel == null) return;
            else if ( !this.Remote_Goal ) {
                console.log(`datachannel state changed: ` + self.datachannel.readyState);
                this.alertDisConnection();
            }
            if (this.connected) this.connected = false;
        })
    }
    private alertDisConnection() {
        alert("Another Player is Disconnected, return Back to Mode Select Stage.");
        director.loadScene("ModeSelect");
    }
}
