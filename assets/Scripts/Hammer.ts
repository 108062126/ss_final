
import { _decorator, Component, Node, RigidBody2D, AudioClip, Collider2D, IPhysics2DContact, TypeScript, Contact2DType } from 'cc';
import { GameMgr } from './GameMgr';
const { ccclass, property } = _decorator;

@ccclass('Hammer')
export class Hammer extends Component {

    @property(Node)
    Game: Node | null = null;

    game: GameMgr | null = null;

    rb: RigidBody2D | null = null;

    onLoad() {
        this.rb = this.node.getComponent(RigidBody2D)!;
        this.game = this.Game!.getComponent(GameMgr)!;
    }

    start () {
        // [3]
        let collider = this.getComponent(Collider2D);
        if (collider) {
            //collider.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
            //collider.on(Contact2DType.END_CONTACT, this.onEndContact, this);
        }
    }

    update (dt:number){
        
    }

    onBeginContact(self:Collider2D, other:Collider2D, contact:IPhysics2DContact){
    }

    onEndContact(self:Collider2D, other:Collider2D, contact:IPhysics2DContact){
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
