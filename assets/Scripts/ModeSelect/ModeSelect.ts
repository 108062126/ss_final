/* Menu.ts */
// Control the Mode Select scene, which is set at Node "UI"
// Mode: Single / Multi

import { _decorator, Component, Node, CCLoader, EditBox, Button, Label, EventHandler, director, TypeScript, find, Vec3, Vec2, Quat, log, Sprite, resources, Texture2D, SpriteFrame, AudioSource } from 'cc';
import { Record } from '../Record';
const { ccclass, property } = _decorator;

@ccclass('ModeSelect')
export class ModeSelect extends Component {
    
    // Basic Nodes
    @property(Button)   btnSingle: Button | null = null;
    @property(Button)   btnMulti:  Button | null = null;

    @property(Node)     uiSingle: Node | null = null;
    @property(Node)     uiMulti: Node | null = null;

    @property(Button)   btnReturn: Button | null = null;
    @property(Button)   btnShop  : Button | null = null;

    @property(Sprite)   SkinDemo: Sprite | null = null;
    @property(Node)     SkinOp: Node | null = null;

    record: Record = new Record();

    onLoad () {
        this.record = find('Record')?.getComponent(Record)!;
        if(!(this.record!.infoLoaded)){
            this.fetchUserInfo();
        }
        else {
            this.btnSingle!.interactable = true;
            this.btnMulti!.interactable = true;
            this.btnShop!.interactable = true;
            this.btnReturn!.interactable = true;
        }
    }

    // Call the init functions
    start () {
        // Set the mode UI to inactive
        this.uiSingle!.active = false;
        this.uiMulti!.active = false;

        // init buttons
        this.btnSingle!.node.on('click', this.activeSingle, this);
        this.btnMulti!.node.on('click', this.activeMulti , this);

        this.btnReturn!.node.on('click', this.return, this);
        this.btnShop!.node.on('click', this.toShop, this);

        let bgmState = this.record!.getComponent(AudioSource)!.playing;
        if(!bgmState){
            this.record!.getComponent(AudioSource)!.play();
        }

    }

    activeSingle () {
        this.uiSingle!.active = true;
        this.uiMulti!.active  = false;
        this.node.active = false;
        this.SkinOp!.active = true;
        this.SkinDemo!.spriteFrame = this.record!.skinTextures[this.record!.skinIdx];
    }
    activeMulti () {
        this.uiSingle!.active = false;
        this.uiMulti!.active  = true;
        this.node.active = false;
        this.SkinOp!.active = true;
        this.SkinDemo!.spriteFrame = this.record!.skinTextures[this.record!.skinIdx];
    }
    return () {
        if (this.node.active) {
            let result = confirm('Are you sure to leave?');
            if (result) {
                firebase.auth().signOut().then(() => {
                    director.loadScene('Menu');
                })
            }
        }
        else {
            this.uiSingle!.active = false;
            this.uiMulti!.active  = false;
            this.node.active = true;
            this.SkinOp!.active = false;
        }
    }

    toShop () {
        /* script that go to shop */
        director.loadScene('Store');
    }

    changeSkin(event: any, dir: number) {
        //log(this.record!.skinTextures);
        let d = Number(dir);
        this.record!.skinIdx = (this.record!.skinIdx + d + this.record!.skins.length) % this.record!.skins.length;
        this.SkinDemo!.spriteFrame = this.record!.skinTextures[this.record!.skinIdx];
        //log(this.record!.skinIdx);
    }

    fetchUserInfo () {
        let db = firebase.database();
        let user = firebase.auth().currentUser;
        if(user){
            db.ref("userInfo/" + user.uid).once('value').then((res: any)=>{
                let data = res.val();
                this.record.displayName = user.email.split('@', 1)[0];
                this.record.stones = data.stones;
                this.record.hasRecord = data.hasRecord == 0 ? false : true;

                let skinArr = data.skins.split(',');
                let i = "";
                for(i in skinArr){
                    this.record!.skins.push(Number(skinArr[i]))

                    resources.load("images/" + skinArr[i] + "/spriteFrame", SpriteFrame ,(err: any, spFrame: SpriteFrame) => {
                        this.record!.skinTextures.push(spFrame);
                    });
                }

                if(data.hasRecord == 1){
                    db.ref("records/" + user.uid).once('value').then((_res: any)=>{
                        let _data = _res.val();

                        this.record.lastTime = _data.time;

                        let pos = new Vec3(_data.position.x, _data.position.y, 0);
                        this.record.lastPosition = pos;

                        let rot = new Vec3(_data.rotation.x, _data.rotation.y, _data.rotation.z);
                        this.record.lastRotation = rot;

                        let hPos = new Vec3(_data.hammerPos.x, _data.hammerPos.y, _data.hammerPos.z);
                        this.record.lastHammerPosition = hPos;

                        this.btnSingle!.interactable = true;
                        this.btnMulti!.interactable = true;
                        this.btnShop!.interactable = true;
                        this.btnReturn!.interactable = true;

                        this.record.infoLoaded = true;
                    });
                }
                else {
                    this.btnSingle!.interactable = true;
                    this.btnMulti!.interactable = true;
                    this.btnShop!.interactable = true;
                    this.btnReturn!.interactable = true;

                    this.record.infoLoaded = true;
                }
            });
        }
    }
}
