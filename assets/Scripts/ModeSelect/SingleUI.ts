import { _decorator, Component, Node, find, Button, Sprite, director, AudioSource } from 'cc';
import { Record } from '../Record';
const { ccclass, property } = _decorator;

@ccclass('SingleUI')
export class SingleUI extends Component {

    @property(Button)
    newGame: Button | null = null;

    @property(Button)
    lastRec: Button | null = null;
    
    record: Record | null = null;

    onLoad() {
        this.record = find('Record')!.getComponent(Record);
        this.record!.isMulti = false;
    }

    start() {
        this.newGame!.interactable = true;
        this.lastRec!.interactable = this.record!.hasRecord;
    }

    startNewGame() {
        this.record!.startLast = false;
        director.loadScene("main");
    }

    startLastRecord() {
        this.record!.startLast = true;
        director.loadScene("main");
    }
}
