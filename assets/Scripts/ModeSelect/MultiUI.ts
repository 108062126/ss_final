/* MultiUI.ts */
// This script is in ModeSelect / MultiUI

import { _decorator, Component, Script, Node, Button, EditBox, Label, TERRAIN_HEIGHT_BASE, find, director } from 'cc';
import { Record } from '../Record';
const { ccclass, property } = _decorator;

@ccclass('MultiUI')
export class MultiUI extends Component {
    
    // Basic Nodes
    @property(Button)   btnCreateRoom: Button = new Button();
    @property(Button)   btnJoinRoom:   Button = new Button();

    @property(EditBox)  boxRoomID: EditBox = new EditBox();

    record: Record | null = null;

    private roomId: any = null;     // Current room ID

    onLoad () {
        console.log(this);
        this.record = find('Record')!.getComponent(Record);
    }

    start () {
        // Init Buttons
        this.btnCreateRoom.node.on('click', this.callerSet, this);
        this.btnJoinRoom.node.on('click', this.calleeSet, this);
    }

    // Set the user as caller and changeScene
    callerSet () {
        console.log(this);
        // Record the data
        this.record!.iscaller = true;
        this.record!.isMulti  = true;
        // Change Scene
        director.loadScene("Hall");
    }

    // Set the user as callee and Join the room
    calleeSet () {
        // Get the roomId from editbox
        this.roomId = this.boxRoomID.textLabel!.getComponent(Label)!.string;
        // const for Paths
        const roomRef = firebase.database().ref('rooms/' + `${this.roomId}`);
        
        // Check the room whether exist.
        let roomSnapShot;
        let self = this;
        roomRef.once('value', (snapshot: any) => {
            if (!snapshot.exists() || this.roomId == "") {
                alert('Room does not exist');
            }
            else{
                self.record!.isMulti = true;
                roomSnapShot = snapshot.val();
                console.log(roomSnapShot);

                // Record the data
                self.record!.iscaller = false;
                self.record!.roomId = self.roomId;
                
                // Change Scene
                director.loadScene("Hall");
            }
        })
    }
}
