/* Menu.ts */
// Control the start scene of the whole game, which is set at Node "UI"

import { _decorator, Component, Node, CCLoader, EditBox, Button, Label, EventHandler, director } from 'cc';
const {ccclass, property} = _decorator;

@ccclass
export class MenuUI extends Component {

    // Basic Nodes
    @property(EditBox)   boxEmail:      EditBox | null = null;
    @property(EditBox)   boxPassword:   EditBox | null = null;

    @property(Button)    btnLogin:      Button | null = null;
    @property(Button)    btnSignup:     Button | null = null;
    @property(Button)    btnLoginGoogle:Button | null = null;

    // Call the init functions
    start () {
        this.btnLogin!.node.on('click', this.Login, this);
        this.btnSignup!.node.on('click', this.Signup, this);
        this.btnLoginGoogle!.node.on('click', this.LoginGoogle, this);
    }

    // Callback Functions
    Login (button: Button) {
        let userEmail    = this.boxEmail!.textLabel!.getComponent(Label)!.string;       // Get User informations
        let userPassword = this.boxPassword!.textLabel!.getComponent(Label)!.string; 

        firebase.auth().signInWithEmailAndPassword(userEmail, userPassword) // Login through Firebase.auth()
        .then(function () {
            console.log("Log In Success:" + userEmail);

            // change scene to xxx
            director.loadScene("ModeSelect");
        })
        .catch(function (error: any) {
            console.log("Error: " + error.message);
            alert("Error: " + error.message);
        }); 0
    }
    Signup (button: Button) {
        let userEmail    = this.boxEmail!.textLabel!.getComponent(Label)!.string;        // Get User informations
        let userPassword = this.boxPassword!.textLabel!.getComponent(Label)!.string;

        // Using outside define to access the right "this" object
        let self = this;
        firebase.auth().createUserWithEmailAndPassword(userEmail, userPassword) // Sign Up to Firebase.auth()
        .then(function (res: any) {
            console.log("Sign Up Success: " + userEmail);
            alert("Sign Up Success: " + userEmail);
            //set init user info
            let user = res.user;
            let d = {
                hasRecord: 0,
                stones: 0,
                skins: "1"
            };
            firebase.database().ref('userInfo/' + user.uid).set(d);

        })
        .catch(function (error: any) {
            console.log("Error: " + error.message);
            alert("Error: " + error.message);
        })
        .then(() => {
            self.boxEmail!.string = "";
            self.boxPassword!.string = "";
        })
    }
    LoginGoogle (button: Button) {
        let provider = new firebase.auth.GoogleAuthProvider();  // Get the GoogleAuth object

        firebase.auth().signInWithPopup(provider) // SignIn with GoogleAuth
        .then(function (result: any) {
            console.log("Login In Success: " + result.user.email);
            // change scene to xxx

            //set init user info if new login
            if(result.additionalUserInfo.isNewUser){
                let user = result.user;
                let d = {
                    hasRecord: 0,
                    stones: 0,
                    skins: "1"
                };
                firebase.database().ref('userInfo/' + user.uid).set(d).then(()=>{
                    director.loadScene("ModeSelect");
                });
            }
            else {
                director.loadScene("ModeSelect");
            }

        })
        .catch(function (error: any) {
            console.log("Error: " + error.message);
            alert("Error: " + error.message);
        })
    }
}
