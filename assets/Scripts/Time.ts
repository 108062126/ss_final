
import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Time')
export class Time extends Component {
    @property(Node)
    Camera: Node | null = null;

    start () {
        // [3]
    }

    update (deltaTime: number) {
        this.node.setPosition(this.Camera!.position.x - 230, this.Camera!.position.y + 250, this.node.position.z);
    }
}