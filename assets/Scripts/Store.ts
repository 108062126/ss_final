
import { _decorator, Component, Node, Label, find, UITransform, Vec2, Vec3, director } from 'cc';
import { Record } from './Record';
const { ccclass, property } = _decorator;

@ccclass('Store')
export class Store extends Component {
    
    @property(Node)
    Label: Node | null = null;

    record: Record | null = null;

    dirty: boolean = true;

    t: boolean = true;

    onLoad() {
        this.record = find('Record')!.getComponent(Record);
        console.log(this.record!.skins);
        console.log(this.record!.skinTextures);
    }

    start () {
    }

    backToMenu() {
        director.loadScene('ModeSelect');
    }

    update (deltaTime: number) {
        if(this.dirty || this.t){
            this.Label!.getComponent(Label)!.string = this.record!.stones.toString();
            this.Label!.setPosition(new Vec3(-this.Label!.getComponent(UITransform)!.contentSize.x, 0, 0));
            if(this.dirty)this.dirty = false;
            else this.t = false;
        }
    }

    pullGacha(){
        console.log('pull');
        if(this.record!.stones >= 3){
            this.record!.stones -= 3;
            firebase.database().ref('userInfo/' + firebase.auth().currentUser.uid + '/stones').set(this.record!.stones).then(()=>{
                this.dirty = true;
                director.loadScene('Gacha');
            });
        }
    }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
