/* HallUI.ts */

import { _decorator, Component, Node, Button, Label, find, director, TERRAIN_HEIGHT_BASE, AudioSource } from 'cc';
import { webRTC } from '../webRTC';
import { Record } from '../Record';
const { ccclass, property } = _decorator;

@ccclass('HallUI')
export class HallUI extends Component {

    /* Basic Components */
    @property(Button)   btn: Button| null = null;
    @property(Node)     nodeRTC: Node| null = null;

    @property(Label)    ownner: Label| null = null;
    @property(Label)    guest:  Label| null = null;

    RTC: webRTC| null = null;   // webRTC component
    RCD: Record| null = null;   // Record component
    RoomRef: any| null = null;  // firebase room reference

    private isReady: boolean| boolean = false;

    public setReady () {
        console.log('isReady');
        this.isReady = true;
    }
    public setGuest (email: string) {
        console.log('Guest: ', email);
        this.guest!.string = email.split('@')[0];

        this.RCD!.player1 = firebase.auth().currentUser.email;
        this.RCD!.player2 = email;
    }
    public changeScene () {
        director.loadScene("main-multi");
    }

    /* Node's Life Cycle */
    onLoad () {
        this.RTC = this.nodeRTC!.getComponent(webRTC);
        this.RCD = find('Record')!.getComponent(Record);
        this.RoomRef = firebase.database().ref('rooms/' + this.RCD!.roomId);
    }
    start () {
        if(this.RCD!.iscaller)
            this.callerInit();
        else
            this.calleeInit();
    }
    update (dt: number) {
        if (this.isReady && this.RCD!.iscaller && this.RTC!.connected) {
            this.btn!.interactable = true;
        }
    }

    private callerInit () {
        this.btn!.interactable = false;
        this.btn!.node.on('click', this.callerBtn, this);
        this.btn!.getComponentInChildren(Label)!.string = "START";

        let userEmail = firebase.auth().currentUser.email;
        this.ownner!.string = userEmail.split('@')[0];

        this.guest!.string = "...(waiting)";
    }
    private calleeInit () {
        this.btn!.interactable = true;
        this.btn!.node.on('click', this.calleeBtn, this);
        this.btn!.getComponentInChildren(Label)!.string = "READY";

        let userEmail = firebase.auth().currentUser.email;
        this.guest!.string = userEmail.split('@')[0];

        this.RoomRef.once('value', (snapshot: any) => {
            const data = snapshot.val();
            this.ownner!.string = data.ownner.split('@')[0];
        });
    }

    private callerBtn () {
        if (this.RTC!.connected && this.isReady) {
            this.RTC!.SendData(JSON.stringify({
                type: 'Start'
            }))
            this.changeScene();
        }
    }
    private calleeBtn () {
        if (this.RTC!.connected) {
            this.btn!.interactable = false;
            
            this.setReady();
            this.RTC!.SendData(JSON.stringify({
                type: 'Ready',
                email: firebase.auth().currentUser.email
            }))
        }
    }
}
