
import { _decorator, Component, Node, Material, AudioSource, resources, Texture2D, SpriteFrame, Sprite, log, MeshRenderer, RenderTexture, Vec3, ParticleSystem, Animation, systemEvent, SystemEventType, director, find, AudioClip } from 'cc';
import { Record } from './Record';
const { ccclass, property } = _decorator;

@ccclass('Gacha')
export class Gacha extends Component {
    
    @property(MeshRenderer)
    cardFront: MeshRenderer | null = null;

    @property(MeshRenderer)
    cardBack: MeshRenderer | null = null;

    @property(Node)
    Card: Node | null = null;

    @property(Node)
    Front: Node | null = null;

    @property(Node)
    Phase1: Node | null = null;

    @property(Node)
    Phase2: Node | null = null;

    @property(Node)
    Phase3: Node | null = null;

    @property(Node)
    Phase4: Node | null = null;

    @property(Node)
    Phase5: Node | null = null;

    @property(Node)
    Phase6: Node | null = null;

    @property(Node)
    balls: Node | null = null;

    @property(Node)
    p2MainRing: Node | null = null;

    @property(Node)
    subRingTop: Node | null = null;

    @property(Node)
    subRingBot: Node | null = null;

    soundEffect: AudioSource | null = null;

    bitmap: any = null;

    ballsSize: number  = 1;

    rotSpeed: number = 0;

    rotSpeedP2: number = 0;

    cardAnim: Animation | null = null;

    done: boolean = false;

    record: Record | null = null;

    onLoad() {
        this.record = find('Record')!.getComponent(Record);
        this.cardAnim = this.Card!.getComponent(Animation);
        this.soundEffect = this.node!.getComponent(AudioSource);

        let rng = Math.floor(Math.random() * 21);
        resources.load("images/" + rng.toString() + "_card/texture", Texture2D ,(err: any, texture: Texture2D) => {
            this.cardFront!.getMaterial(0)!.setProperty('albedoMap', texture);
        });
        if(!this.record!.skins.includes(rng)){
            console.log('new skin');
            this.record!.skins.push(rng);

            let uid = firebase.auth().currentUser.uid;
            firebase.database().ref('userInfo/' + uid + '/skins').set(this.record!.skins.join());

            resources.load("images/" + rng.toString() + "/spriteFrame", SpriteFrame ,(err: any, spFrame: SpriteFrame) => {
                this.record!.skinTextures.push(spFrame);
            });
        }
    }

    start () {
        this.record!.getComponent(AudioSource)!.stop();
        this.scheduleOnce(()=>{
            this.initPhase1();
        }, 1);
    }

    update (deltaTime: number) {
    }

    initPhase1(){
        this.soundEffect!.play();

        this.Phase1!.active = true;
        
        let that = this;
        let spread = function() {
            that.ballsSize += 0.03;
            that.balls!.setScale(new Vec3(that.ballsSize, that.ballsSize, 0.01));
        }
        this.scheduleOnce(()=>{
            this.schedule(spread, 0.03);
            this.scheduleOnce(()=>{
                this.unschedule(spread);
                this.balls!.getComponent(ParticleSystem)!.rotationOvertimeModule!.enable = true;
                this.schedule(()=>{
                    this.rotSpeed += 0.3;
                    this.balls!.setRotationFromEuler(new Vec3(90, this.rotSpeed * this.rotSpeed, 0));
                }, 0.03);
                this.scheduleOnce(()=>{
                    this.initPhase2();
                }, 1);
            }, 2);
        }, 2);
    }

    initPhase2(){
        this.Phase2!.active = true;

        let that = this;
        this.schedule(()=>{
            if(this.rotSpeedP2 < 50){
                this.rotSpeedP2 += 0.3;
            }
            else {
                this.rotSpeedP2 /= 2;
            }
            this.p2MainRing!.setRotationFromEuler(new Vec3(90, this.rotSpeedP2 * this.rotSpeedP2, 0));
            this.subRingBot!.setRotationFromEuler(new Vec3(90, this.rotSpeedP2 * this.rotSpeedP2, 0));
            this.subRingTop!.setRotationFromEuler(new Vec3(90, this.rotSpeedP2 * this.rotSpeedP2, 0));
        }, 0.03);

        this.scheduleOnce(()=>{
            this.p2MainRing!.getComponent(Animation)!.play();
            this.subRingBot!.active = true;
            this.subRingTop!.active = true;

            this.subRingBot!.getComponent(Animation)!.play();
            this.subRingTop!.getComponent(Animation)!.play();

            this.p2MainRing!.getComponent(Animation)!.on(Animation.EventType.FINISHED, ()=>{
                this.initPhase3();
            })
        }, 3.5);
    }

    initPhase3(){
        this.Phase3!.active = true;
        this.scheduleOnce(()=>{
            this.initPhase4();
        }, 2);
    }

    initPhase4(){
        this.Card!.active = true;

        let that = this;
        let cardRot = function(){
            let s = that.cardAnim!.getState("Card");
            if(s.speed >= 0.02){
                s.speed = s.speed - 0.007;
            }
            else {
                let r = that.Card!.rotation.getEulerAngles(new Vec3());
                s.speed = Math.min(0.02, (180 - Math.abs(r.y)/360));

                if(r.y < 0.5){
                    that.Card!.setRotationFromEuler(new Vec3());
                    that.cardAnim!.stop();
                    that.unschedule(cardRot);
                    that.initPhase5();
                }
            }
        }
        this.schedule(cardRot, 0.03);

        this.scheduleOnce(()=>{
            this.Phase4!.active = true;
        }, 1.5);
    }

    initPhase5(){
        this.scheduleOnce(()=>{
            this.Phase5!.active = true;
            this.Front!.getComponent(Animation)!.play();
            this.Front!.getComponent(Animation)!.on(Animation.EventType.FINISHED, ()=>{
                this.initPhase6();
            })
        }, 1);
    }

    initPhase6(){
        this.scheduleOnce(()=>{
            this.Phase6!.active = true;
            this.cardAnim!.play('Standout');
            this.cardAnim!.getComponent(Animation)!.on(Animation.EventType.FINISHED, ()=>{
                log('done');
                this.scheduleOnce(()=>{
                    director.loadScene('Store');
                }, 2);
            })
        }, 0.5);
    }

    phase1Spread(){
        this.ballsSize += 0.01;
        this.balls!.setScale(new Vec3(this.ballsSize, this.ballsSize, 0.01));
    }

}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.0/manual/en/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.0/manual/en/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.0/manual/en/scripting/life-cycle-callbacks.html
 */
