
import { _decorator, Component, Node, Label, log, Vec3, Vec2, EventMouse, systemEvent, SystemEventType, UITransform, Script, TypeScript, RigidBody2D, Slider, AudioSource, AudioClip, director, Director, find, Button, EventHandler, Quat, TERRAIN_HEIGHT_BASE} from 'cc';
import { Hammer } from './Hammer'
import { Player } from './Player';
import { OtherPlayer } from './OtherPlayer';
import { Record } from './Record';
import { CursorLocker } from './CursorLocker';
import { webRTC } from './webRTC';

const { ccclass, property } = _decorator;

@ccclass('GameMgr')
export class GameMgr extends Component {
    @property(Node)
    OtherPlayer: Node | null = null;

    @property(Node)
    Player: Node | null = null;

    @property(Node)
    Hammer: Node | null = null;

    @property(Node)
    Joint: Node | null = null;

    @property(Node)
    Intro: Node | null = null;

    @property(Node)
    ResumeBtn: Node | null = null;

    @property(Node)
    TimeLabel: Label | null = null;

    mousePos = new Vec2();

    deltaPos = new Vec2();

    hammer: Hammer | null = null;

    audioSource: AudioSource | null = null;
    
    started: boolean = false;

    record: Record | null = null;

    time: number = 0;

    cLocker: CursorLocker | null = null;

    goal: boolean | boolean = false;

    gameFinish: boolean | boolean = false;  // This variable to avoid call finishGame() multiple times

    RTC: webRTC | null = null;

    callerPos: Vec3 | null = null;

    calleePos: Vec3 | null = null;

    onLoad(){
        this.hammer = this.Hammer!.getComponent(Hammer)!;
        this.audioSource = this.node.getComponent(AudioSource)!;
        this.record = find('Record')!.getComponent(Record);
        this.cLocker = find('CursorLock')!.getComponent(CursorLocker);
        if (this.record!.isMulti)
            this.RTC = find('webRTC')!.getComponent(webRTC);
        this.callerPos = new Vec3(-240, -110, 0);
        this.calleePos = new Vec3(5, -120, 0);
    }

    start(){
        this.record!.node.getComponent(AudioSource)!.stop();
        this.audioSource!.play();
        this.cLocker!.setStart(true);
        if (this.record!.isMulti)
            this.RTC!.setInGame();

        let e = new EventHandler();
        e.target = this.cLocker!.node;
        e.component = 'CursorLocker';
        e.handler = 'lockPointer';

        this.Intro!.getComponent(Button)!.clickEvents.push(e);
        this.ResumeBtn!.getComponent(Button)!.clickEvents.push(e);

        if(this.record!.hasRecord && this.record!.startLast){
            this.time = this.record!.lastTime;
        }
    }

    RealTimeUpdate (remotetime: number) {
        this.time = remotetime > this.time ? remotetime : this.time;
        this.TimeLabel!.getComponent(Label)!.string = "Time: " + Math.floor(this.time).toString();
    }

    update(dt: number){
        if (!this.goal) {
            if (this.record!.isMulti) {
                this.time += dt;
                this.TimeLabel!.getComponent(Label)!.string = "Time: " + Math.floor(this.time).toString();
            }
            else if (this.cLocker!.locked) {
                this.time += dt;
                this.TimeLabel!.getComponent(Label)!.string = "Time: " + Math.floor(this.time).toString();
            }
        }
        
        /* Goal check */
        if (this.record!.isMulti)
            this.goal = this.Player!.getComponent(Player)!.goal && this.OtherPlayer!.getComponent(OtherPlayer)!.goal;
        else
            this.goal = this.Player!.getComponent(Player)!.goal;
        
        if(this.goal)
            this.finishGame();
    }

    toggleVolume(slider: Slider) {
        this.audioSource!.volume = slider.progress;
    }

    playEffect(sound: AudioClip, vol: number) {
        this.audioSource!.playOneShot(sound, vol);
    }

    quitGame() {
        if(this.started && !this.record!.isMulti){
            let db = firebase.database();
            let rot = this.Joint!.getRotation(new Quat()).getEulerAngles(new Vec3());
            let pos = this.Player!.position;
            let hPos = this.Hammer!.position;

            let rec = {
                position: {
                    x: pos.x,
                    y: pos.y
                },
                rotation: {
                    x: rot.x,
                    y: rot.y,
                    z: rot.z
                },
                hammerPos: {
                    x: hPos.x,
                    y: hPos.y,
                    z: hPos.z
                },
                time: this.time
            };

            let uid = firebase.auth().currentUser.uid;

            this.record!.lastTime = this.time;
            this.record!.lastPosition = pos;
            this.record!.lastRotation = new Vec3(rot.x, rot.y, rot.z);
            this.record!.lastHammerPosition = hPos;
            this.record!.hasRecord = true;

            this.cLocker!.setStart(false);

            db.ref('userInfo/' + uid + '/hasRecord').set(1).then(()=>{
                db.ref('records/' + uid).set(rec).then(()=>{
                    director.loadScene("ModeSelect");
                });
            });

        }
        else {
            director.loadScene("ModeSelect");
        }
    }

    finishGame(){
        if (this.gameFinish) return;

        this.gameFinish = true;
        this.record!.time = this.time;

        if (this.record!.isMulti)
        this.RTC!.SendData(JSON.stringify({type: 'victory'}));

        const userID  = firebase.auth().currentUser.uid;
        const roomRef = firebase.database().ref('userInfo/' + userID);
        
        /* Update Player information */
        const info = {
            stones: this.record!.stones,
            hasRecord: 0
        };
        if (!this.record!.isMulti)  roomRef.update(info);
        
        /* Update Players newRecord */
        let playerStr: String;
        if (this.record!.isMulti)
            playerStr = this.record!.player1.split('@')[0] + this.record!.player2.split('@')[0];
        else playerStr = firebase.auth().currentUser.email;

        const newRecord = {
            player: playerStr,
            time: this.time
        }
        if (this.record!.isMulti)
        {   // Multiplay
            if (this.record!.iscaller) {
                firebase.database().ref('RankingMulti').push(newRecord).then(() => {
                    document.exitPointerLock();
                    director.loadScene("Finish");
                })
            }
            else {
                document.exitPointerLock();
                director.loadScene("Finish")
            }
        }
        else
        {   // Singleplay
            firebase.database().ref('Ranking').push(newRecord);
            firebase.database().ref('userInfo/' + userID + '/selfRecord').push(newRecord).then(() => {    
                document.exitPointerLock();
                director.loadScene("Finish"); 
            })
        }
    }
}
